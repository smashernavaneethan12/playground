import UIKit


enum UserErros : String, Error {
    case nullError = "nil value occured"
    case userError = "user inputs are same"
    
}

func addTwoNum(a:Int?,b:Int?) throws -> Int{
    guard let a = a, let b = b else {
        throw UserErros.nullError
    }
    if a == b {
        throw UserErros.userError
    }
    else{
        let n1 = a
        let n2 = b
        return n1 + n2
    }
}

do {
    var a : Int
    try a = addTwoNum(a: 10, b: 20)
    print(a)
    
} catch  UserErros.userError{
    print(UserErros.userError.rawValue)
} catch UserErros.nullError{
    print(UserErros.nullError.rawValue)
}

